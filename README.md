#### 项目运行：
1. 下载项目。
2. 使用HbuilderX导入项目
3. 打开manifest.json->uni-app应用标识，生成自己的标识
4. 打包自定义基座->得到自定义基座->然后运行时选择自定义基座->运行项目

> 官方文档：[HBuilderX使用本地插件](https://nativesupport.dcloud.net.cn/NativePlugin/use/use_local_plugin)，[原生插件](https://uniapp.dcloud.io/plugin/native-plugin.html)